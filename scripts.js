
function isEmail($email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,6})+$/;
  return regex.test($email);
}

$('#submit').click(function(){
	//Checks if email and password are OK
	
	if ($('#email').css("border") === "1px solid rgb(255, 0, 0)" || $('#email').val().length === 0) {
		alert("EMAIL INVÁLIDO");
	}
	else if ($('#password').css("border") === "1px solid rgb(255, 0, 0)" || $('#password').val().length === 0
		|| $('#confirm').css("border") === "1px solid rgb(255, 0, 0)" || $('#confirm').val().length === 0) {
		alert("PROBLEMA COM A SENHA");
	}
	else {
		alert ("CADASTRO EFETUADO COM SUCESSO. TODO: IMPLEMENTAR ENVIO DO FORMULÁRIO")
	}
});			


$("#email").on("change paste keyup cut", function() {
	if (!isEmail($("#email").val()) ){
		$('#email').css("border", "1px solid red");
	}
	else {
		$('#email').css("border", "1px solid #1ed699");
	}
});

$("#password").on("change paste keyup cut", function() {
	var is1right, is2right, is3right;
   //checks password lenght
   if($('#password').val().length <6) {
   		$(".ping-1").css("color","red");
   		is1right = 0;
   }
   else {
   		$(".ping-1").css("color","#1ed699");
   		is1right = 1;
   }

   //checks uppercase
   if($('#password').val().toLowerCase() === $('#password').val()) {
   		$(".ping-2").css("color","red");
   		is2right = 0;
   }
   else {
  	 	$(".ping-2").css("color","#1ed699");
   		is2right = 1;
   }

   //checks numbers
   var text = $('#password').val().match(/\d+/g);
 		if (text === null) {
 		$(".ping-3").css("color","red");
 		is3right = 0;
   }
   else {
   		$(".ping-3").css("color","#1ed699");
   		is3right = 1;
   }
   var certos = is1right + is2right + is3right;
   if (certos ===1) {
   		$('.check-1').css("background-color","#f26722");
   		$('.check-2').css("background-color","#ededed");
   		$('.check-3').css("background-color","#ededed");   		
   		$('#password').css("border","1px solid #f26722");

   }
   else if (certos ===2) {
		$('.check-1').css("background-color","#f2b822");
   		$('.check-2').css("background-color","#f2b822");
   		$('.check-3').css("background-color","#ededed");   		
   		$('#password').css("border","1px solid #f2b822");
   }
   else if (certos ===0) {
   		$('.check-1').css("background-color","#ededed");
   		$('.check-2').css("background-color","#ededed");
   		$('.check-3').css("background-color","#ededed");   		
   		$('#password').css("border","1px solid red");
   }
   else {
   		$('.check-1').css("background-color","#1ed699");
   		$('.check-2').css("background-color","#1ed699");
   		$('.check-3').css("background-color","#1ed699");   		
   		$('#password').css("border","1px solid #1ed699");

   }

//updates the verification field
   if ($('#confirm').val() != $('#password').val()) {
		$('#confirm').css("border","1px solid red");
		hasError = true;
	}
	else {
		$('#confirm').css("border","1px solid #1ed699");
		if (certos=== 3) {
			hasError = false;
		}
		else {
			hasError = true;
		}
	}



});		

//checks if verification field equals entered password
$("#confirm").on("change paste keyup cut", function() {
	if ($('#confirm').val() != $('#password').val()) {
		$('#confirm').css("border","1px solid red");
	}
	else {
		$('#confirm').css("border","1px solid #1ed699")
	}

});	